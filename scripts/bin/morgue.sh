#!/bin/bash
#
# Extract the characters of the "morgue" files of "crawl" and count them.
#
# Copyright (C) 2017 Maximilian Wuttke
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

case "$(uname -s)" in
    Linux*)     crawl_path=$HOME/.crawl;;
    Darwin*)    crawl_path=$HOME/Library/Application\ Support/Dungeon\ Crawl\ Stone\ Soup;;
    *)          crawl_path=$HOME/.crawl
esac

function getChar() {
	# Get the line with the turn number (and the character description in parentheses)
	grep -E "Turns: [0-9]*" "$1" | head -1 |
	# Extract the character description in parentheses (Each of the two words gets on its own line)
	grep -Eo '\(.*\)' | grep -Eo '[a-zA-Z ]+'
}

for i in "$crawl_path"/morgue/*.txt; do
	getChar "$i"
done | sort | uniq -c | sort --numeric-sort |
awk '{ sum+=$1; print} END { print "Total: " sum }'

# XXX (works for me)
echo "Won Games: 0"
