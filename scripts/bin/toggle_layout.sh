#!/bin/bash

function getCurrentLayout() {
	setxkbmap -query | awk '/layout/{print $2}'
}

function setLayout() {
	setxkbmap $@
	echo "Changed to $@."
}

case $(getCurrentLayout) in
	de) setLayout us euro;;
	us) setLayout de;;
esac
