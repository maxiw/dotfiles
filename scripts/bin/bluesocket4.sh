#!/bin/bash
# Login/logout from bluesocket4 network (e.g. Saarland University LAN)

# The POST URL for login
LOGIN_URL="https://Bluesocket4.net.uni-saarland.de/login.pl"

# The GET URL for logout
LOGOUT_URL="log.out"


# Username and password
VPNC_CONF="/etc/vpnc/uds.conf"
USERNAME=$(cat "$VPNC_CONF" | grep "Xauth username" | cut -d' ' -f 3 | cut -d'@' -f1)
PASSWORD=$(cat "$VPNC_CONF" | grep "Xauth password" | cut -d' ' -f3)

# LAN Host IP
IP=$(ip addr show enp4s0f2 | grep inet | head -1 | grep --only-matching '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*' | head -1)

function log_out() {
	curl --silent --insecure -L "$LOGOUT_URL" >/dev/null
	echo "Logged out"
}

function log_in() {
	curl --silent --insecure --form "_FORM_SUBMIT=1" --form "which_form=reg" --form "destination=" \
		--form "source=$IP" --form "bs_name=$USERNAME" --form "bs_password=$PASSWORD" "$LOGIN_URL" >/dev/null
	echo "Logged in"
}

function usage() {
	echo "Usage: $0 [login|logout]"
}

case $1 in
	logout) sudo vpnc-disconnect; log_out;;
	login) log_in;;
	vpnc) log_in; sudo vpnc "$VPNC_CONF" ;;
	*) log_in;;
esac
