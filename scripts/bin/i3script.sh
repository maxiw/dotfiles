#!/bin/bash
#
# Script for i3bar
#
# Purpose: Insert the keybout layout

# line 1 contains version, line 2 contains "[" and the folowing lines
# contain the content
i3status | (read line1 && echo $line1 && read line2 && echo $line2 && while true
do
	read line3
	# Get the string to insert
	Layout=$(setxkbmap -query | awk '/layout/{print $2}')
	case $Layout in
		de)
			dat="{ \"full_text\": \"Layout: $Layout\", \"color\":\"#009E00\" }," ;;
		us)
			dat="{ \"full_text\": \"Layout: $Layout\", \"color\":\"#C60101\" }," ;;
	esac

	# Insert "$dat" after the 9 "{"
	sed "s/{/\n{/g" <<<"$line3" | sed "9 a $dat"
done)
