#/usr/bin/env awk
#
# Compute the median of a sorted list of numbers
# Usage: sort -n | awk -f median.awk
#
# Source: <https://im-coder.com/median-der-spalte-mit-awk.html>
{
    count[NR] = $1;
}
END {
    if (NR % 2) {
        print count[(NR + 1) / 2];
    } else {
        print (count[(NR / 2)] + count[(NR / 2) + 1]) / 2.0;
    }
}
