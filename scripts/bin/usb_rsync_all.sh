#!/bin/bash
shopt -s expand_aliases
alias rsync_usb='rsync --modify-window=1 -aP'
rsync_usb ~/Sync/default ~/mnt/usb/Sync/
# To sync in the other direction, use the following:
# rsync_usb ~/mnt/usb/Sync/default ~/Sync/
