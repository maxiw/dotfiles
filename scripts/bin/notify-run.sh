#!/bin/bash
#
# Run a command and send a desktop notification after the command is finished

time $@
notify-send -u normal "Command finished" "$(echo $@)"

# Below, we use the GNU version of `time', not the shell command
# time=$(/usr/bin/time -f '%E' $@ 2>&1 1>/dev/null)
# notify-send -u normal "Command finished after $time" "$(echo $@)"
