# ~/.bashrc: executed by bash(1) for non-login shells.
# Based on some Debian's .bashrc


# TexLive
export MANPATH=$MANPATH:/usr/local/texlive/2015/texmf-dist/doc/man
export INFOPATH=$INFOPATH:/usr/local/texlive/2015/texmf-dist/doc/info
export PATH=$PATH:/usr/local/texlive/2015/bin/x86_64-linux/

# Eclipse
export PATH=$PATH:/opt/eclipse

# Geogebra
export PATH=$PATH:/opt/GeoGebra/geogebra/

# Tam
alias tam='xset b && alert -r 42'

# Go
export GOPATH=~/go
export PATH=$PATH:~/go/bin

# SML-NJ
export PATH="$PATH:/usr/lib/smlnj/bin"
export SMLNJ_HOME=/usr/lib/smlnj/

# Vim
export VISUAL=vim

# Android Stuff
export ANDROID_HOME=/home/maxi/android-sdk-linux
export ANDROID_NDK_ROOT=/home/maxi/android-ndk-r11b
export NDK_TARGET=arm-linux-androideabi-4.9

# Aliases
export GPG_KEY_SERVER="hkp://pool.sks-keyservers.net"
alias gpg2="gpg2 --keyserver $GPG_KEY_SERVER"
alias gpg="gpg --keyserver $GPG_KEY_SERVER"
alias l=ls
alias cd..='cd ..'
alias cls=clear
alias grep='grep --color'
alias ls='ls -A --color'

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

source /home/maxi/.config/broot/launcher/bash/br
