set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'cypok/vim-sml'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'preservim/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'dag/vim-fish'
"" Currently unused plugins
" Plugin 'fatih/vim-go'
" Plugin 'mwuttke97/vim-pseuco'
" Plugin 'dpelle/vim-LanguageTool'
" Plugin 'lervag/vimtex'
" Plugin 'bohlender/vim-smt2'
call vundle#end()


""
"" Options
""

" Enable mouse
set mouse=a

" Line wrapping (like `visual-line-mode` in Emacs)
set linebreak


""
"" Plugins
""

" Open/close NERD tree window with <C-n>
map <C-n> :NERDTreeToggle<CR>

" CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'


"" Airline config

" let g:airline_theme = 'raven'
" let g:airline_theme = 'ravenpower'
let g:airline_theme = 'minimalist'
let g:airline_powerline_fonts = 1


""
"" Programming language specific settings
""

" Lua: Only two spaces
autocmd Filetype lua setlocal tabstop=2
autocmd Filetype lua setlocal softtabstop=2
autocmd Filetype lua setlocal shiftwidth=2
autocmd Filetype lua setlocal expandtab
