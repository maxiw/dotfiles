if status is-interactive
    # Commands to run in interactive sessions can go here
end

#
# Path
#
fish_add_path ~/.local/bin
fish_add_path ~/bin
fish_add_path ~/.cabal/bin
fish_add_path ~/.cargo/bin
export INFOPATH="$INFOPATH:/usr/share/info"

# Flatpak
export XDG_DATA_DIRS="$XDG_DATA_DIRS:$HOME/.local/share/flatpak/exports/share"


# Set nvim as default editor
export VISUAL="nvim"
export EDITOR="$VISUAL"

#
# Aliases
#
alias vim='nvim'
alias info='info --vi-keys'
alias z="zathura"
alias f="feh --full-screen --auto-zoom --auto-rotate"

export GPG_KEY_SERVER="hkp://pool.sks-keyservers.net"
alias gpg2="gpg2 --keyserver $GPG_KEY_SERVER"
alias gpg="gpg --keyserver $GPG_KEY_SERVER"

alias wlan-off='iwctl device wlan0 set-property Powered off'
alias wlan-on='iwctl device wlan0 set-property Powered on'

# Useful for synchronization to/from FAT USB devices
alias rsync_usb='rsync --modify-window=1 -aP'

# Download videos and create a desktop notification when done
# Use the flag `-x` for audio-only.
alias ytdl="notify-run.sh youtube-dl"


#
# SSH
#

# Gnupg agent (for PGP smart card)
set -e SSH_AGENT_PID
if not set -q gnupg_SSH_AUTH_SOCK_by or test $gnupg_SSH_AUTH_SOCK_by -ne $fish_pid
    set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
end
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin $PATH /home/maxi/.ghcup/bin # ghcup-env
