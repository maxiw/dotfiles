# This makes the computer *not* wake from stand by when I move the mouse a press a key
# (Not even if the laptop lid is openeed)
function no_wakeup
    sudo -v && \
    for i in (cat /proc/acpi/wakeup | grep enabled | cut -f1)
        echo "$i" | sudo tee /proc/acpi/wakeup
    end
end
