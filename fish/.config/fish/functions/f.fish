# Defined in - @ line 1
function f --wraps='feh --full-screen --auto-zoom --auto-rotate' --description 'alias f=feh --full-screen --auto-zoom --auto-rotate'
  feh --full-screen --auto-zoom --auto-rotate $argv;
end
