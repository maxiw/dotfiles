;;
;; Melpa
;;

;; That seems to be a be a package archive or something.
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)


;;
;; AUCTeX Configuration
;;

(setq TeX-auto-save t)
(setq TeX-parse-self t)

;; ;; Wrap the display of words for long lines.
;; (add-hook 'tex-mode-hook 'visual-line-mode)
;;
;; ;; Highlight To-Do comments and make them searchable
;; (add-hook 'tex-mode-hook 'comment-tags-mode)

;; Obviously use reftex in LaTeX files
(add-hook 'LaTeX-mode-hook 'reftex-mode)

;; Also use company in LaTeX files
(add-hook 'LaTeX-mode-hook 'company-mode)
(add-hook 'LaTeX-mode-hook 'company-statistics-mode)

;; This is a hack to overwrite the existing hook for "definition"
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (LaTeX-add-environments
	     '("lemma" LaTeX-env-args ["Lemma header"]))))

;; This enables SyncTeX (jumping to the position in the pdf viewer and jumping back to the source code)
(add-hook 'TeX-mode-hook 'TeX-source-correlate-mode)

;; This enables the "flyspell" mode. This mode underlines typos
(add-hook 'TeX-mode-hook 'flyspell-mode)

;; line wrapping (like `set linebreak` in vim)
(add-hook 'TeX-mode-hook 'visual-line-mode)

;; (add-hook 'TeX-mode-hook 'company-auctex-init)



(defun go-to-viewer (name)
  "search and focus a window with title [name], and move the
  mouse away"
  (call-process "xdotool" nil nil nil "mousemove" "10000" "10000")
  (call-process "xdotool" nil nil nil "search" "--name" name "windowfocus")
  nil)

(defun TeX-view-focus ()
  "Open and focus the pdf viewer for the current LaTeX file."
  (interactive)
  (let ((pdf-file (concat (TeX-master-file) ".pdf")))
    (go-to-viewer pdf-file)
    (TeX-view)))


(add-hook 'TeX-mode-hook
	  (lambda ()
	    (define-key LaTeX-mode-map (kbd "C-c C-v") 'TeX-view-focus)))

;; This is a stupid feature
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (LaTeX-add-environments
	     '("stupid" LaTeX-env-stupid nil nil))))


(defun LaTeX-env-stupid (environment &optional unused1 &rest unused2)
  (print "This is a stupid feature"))


;; (defun LaTeX-env-args-label (environment &optional unused)
;;   ;; Add an environment with arguments and a label
;;   (print "This should work! 55")
;;   (LaTeX-env-args environment ["Heading"])
;;   (when (LaTeX-label environment 'environment)
;;     (LaTeX-newline)
;;     (indent-according-to-mode)))
;;
;; (add-hook 'LaTeX-mode-hook
;;	  (lambda ()
;;	    (LaTeX-add-environments
;;	     '("Lemma" LaTeX-env-args-label foo)
;;	     '("Definition" LaTeX-env-args-label foo))))



(defun LaTeX-env-arg-label (environment args &optional ignore)
  ;; Add an environment with arguments and a label
  (print "This should work! 55")
  (LaTeX-env-args environment args)
  (when (LaTeX-label environment 'environment)
    (LaTeX-newline)
    (indent-according-to-mode)))

(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (LaTeX-add-environments
	     '("Lemma" LaTeX-env-arg-label ["Heading of the Lemma"])
	     '("Definition" LaTeX-env-arg-label ["Heading of the Definition"]))))


(add-hook 'LaTeX-mode-hook 'comment-tags-mode)



;; Save TeX and LaTeX packages and document classes to a file
;; Code inspired by `company-statistics.el'

(defvar LaTeX-packages-file (expand-file-name "latex-packages-cache.el" user-emacs-directory))

(defun LaTeX-save-package-list ()
  "Save TeX and LaTeX packages and document classes to the file `LaTeX-packages-file'"
  (interactive)
  (with-temp-buffer
    (let (print-level print-length)
      (insert
       (format "%S"
	       `(setq LaTeX-global-class-files (quote ,LaTeX-global-class-files)
		      TeX-global-input-files (quote ,TeX-global-input-files))))
      (write-file LaTeX-packages-file nil))))

(defun LaTeX-load-package-list ()
  "Load TeX and LaTeX packages and document classes from the file `LaTeX-packages-file'"
  (interactive)
  (load LaTeX-packages-file 'noerror nil 'nosuffix))

(with-eval-after-load "latex" (LaTeX-load-package-list))


;;
;; RefTeX
;;

;; use j and k in reftex-toc and reftex-select-label
(add-hook 'reftex-toc-mode-hook
	  (lambda ()
	    (define-key reftex-toc-mode-map "q" 'reftex-toc-quit-and-kill)
	    (define-key reftex-toc-mode-map "j" 'reftex-toc-next)
	    (define-key reftex-toc-mode-map "k" 'reftex-toc-previous)))
(add-hook 'reftex-select-label-mode-hook
	  (lambda ()
	    (define-key reftex-select-label-mode-map "j" 'reftex-select-next)
	    (define-key reftex-select-label-mode-map "k" 'reftex-select-previous)))
(add-hook 'reftex-select-bib-mode-hook
	  (lambda ()
	    (define-key reftex-select-bib-mode-map "j" 'reftex-select-next)
	    (define-key reftex-select-bib-mode-map "k" 'reftex-select-previous)))


;; I patchaed the reftex command `reftex-change-label`, so that the
;; default in the second promt is `from` (the value first
;; entered). This makes it easier change the label, e.g. if there just
;; was a typo, so we don't have to type in the rest of the label again
;; (and potentially introduce new typos!)
(defun my-reftex-change-label (&optional from to)
  "Run `query-replace-regexp' of FROM with TO in all macro arguments.
Works on the entire multifile document.
If you exit (\\[keyboard-quit], RET or q), you can resume the query replace
with the command \\[tags-loop-continue].
No active TAGS table is required."
  (interactive)
  (let ((default (reftex-this-word "-a-zA-Z0-9_*.:")))
    (unless from
      (setq from (read-string (format "Replace label globally [%s]: "
				      default))))
    (if (string= from "") (setq from default))
    (unless to
      (setq to (read-string (format "Replace label %s with: "
				    from) from)))
    (reftex-query-replace-document
     (concat "{" (regexp-quote from) "}")
     (format "{%s}" to))))
(add-hook 'reftex-mode-hook (lambda () (fset 'reftex-change-label 'my-reftex-change-label)))



(defun TeX-find-master-log ()
  "Open the log file associated to the TeX project"
  (interactive)
  (find-file (concat (TeX-master-file) ".log")))



;;
;; evil
;;

;; Now installed from Melpa
;; (add-to-list 'load-path "~/.emacs.d/lisp/evil")
(require 'evil)
(evil-mode 1)


;; Remove "C-b" binding (which is bound by default to `evil-scroll-up`, but I don't like this binding)
;; Pressing "C-b" now executes the Emacs default (`backward-char`)
(add-hook 'evil-mode-hook
	  (lambda ()
	    (define-key evil-motion-state-map (kbd "C-b") nil)))

;; Bind `\` to `C-u`, and | to `evil-execute-in-emacs-state` (which is the default for `\`)
(defun my-evil-rebind-universal-argument ()
  (interactive)
  (evil-define-key 'normal 'global (kbd "\\") 'universal-argument)
  (evil-define-key 'normal 'global (kbd "|") 'evil-execute-in-emacs-state))

;; FIXME: This sometimes doesn't work.
;; Should this be in a hook?
(add-hook 'evil-mode-hook 'my-evil-rebind-universal-argument)


;; Modes where evil should not be used.
;; Note: Alternatively, I could use `evil-set-initial-state' or `evil-emacs-state-modes'
;; if I just want that the default mode is "insert".
(add-hook 'term-mode-hook 'turn-off-evil-mode)

;; TODO: I still don't understand how to use `term'.
;; - I can't type `M-x`
;; - Evil doesnt' really work


;; Adding an entry to a big list with `custom' is not nice, since it repeats every element
;; Therefore, I just use `add-to-list' to add the item that I want to add to the list.
;; TODO: Test
(add-to-list 'evil-normal-state-modes 'buffer-menu-mode)


;; Evil text objects for SML-like comments: (* like this *) (*or like this*)
;; Type "vic" or "vac" to select the content of the comment or the entire comment,
;; respectively.
(evil-define-text-object evil-inner-comment (count &optional beg end type)
  "Select inner SML-like comments"
  :extend-selection nil
  (evil-select-paren  "(\\* *" " *\\*)" beg end type count))
(define-key evil-inner-text-objects-map "*" #'evil-inner-comment)
(evil-define-text-object evil-a-comment (count &optional beg end type)
  "Select an SML-like comments"
  :extend-selection nil
  (evil-select-paren  "(\\*" "\\*)" beg end type count t))
(define-key evil-outer-text-objects-map "*" #'evil-a-comment)

;; Other text objects
;; TODO: Support for displayed TeX equations
(evil-define-text-object evil-inner-tex-inline-formula (count &optional beg end type)
  "Select inner dollar"
  :extend-selection nil
  (evil-select-quote ?$ beg end type count))
(define-key evil-inner-text-objects-map "$" #'evil-inner-tex-inline-formula)
(evil-define-text-object evil-a-tex-inline-formula (count &optional beg end type)
  "Select an SML-like comments"
  :extend-selection nil
  (evil-select-quote ?$ beg end type count t))
(define-key evil-outer-text-objects-map "$" #'evil-a-tex-inline-formula)


;;
;; Evil Surround
;;

(global-evil-surround-mode 1)


(add-hook 'evil-surround-mode-hook
	  (lambda ()
	    (add-to-list 'evil-surround-pairs-alist
			 '(?s "⟦" . "⟧") ;; Semantic paranthesis
			 '(?t "⟨" . "⟩")))) ;; fancy products/tuples

;;
;; Proof General
;;

;; Open .v files with Proof General's Coq mode
(load "~/.emacs.d/lisp/PG/generic/proof-site")

;; Autocompleting symbols and tactics defined externally
(setq company-coq-live-on-the-edge t)

;; Load company-coq when opening Coq files
(add-hook 'coq-mode-hook #'company-coq-mode)

;; This is a hack, to enable C-RET in terminals; the terminal emulator has to be configured to send this control sequence when Ctrl+Ret is pressed.
(define-key local-function-key-map "\033[73;5~" [(control return)])

;; Some shortcuts that work with evil mode
(add-hook 'coq-mode-hook
	  (lambda ()
	    (define-key evil-normal-state-map (kbd "M--") 'company-coq-jump-to-definition)
	    (define-key proof-mode-map (kbd "C-c ;") 'comment-or-uncomment-region)))

;; Use company-statistics-mode, which makes writing common unicode symbols much easier
(add-hook 'coq-mode-hook
	  (lambda ()
	    (company-statistics-mode)
            (comment-tags-mode)))


;;
;; OCaml stuff
;;

(load "/home/maxi/.opam/coq88/share/emacs/site-lisp/tuareg-site-file")

(let ((opam-share (ignore-errors (car (process-lines "opam" "config"
						     "var" "share")))))
  (when (and opam-share (file-directory-p opam-share))
    ;; Register Merlin
    (add-to-list 'load-path (expand-file-name "emacs/site-lisp"
					      opam-share))
    (autoload 'merlin-mode "merlin" nil t nil)
    ;; Automatically start it in OCaml buffers
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)
    ;; Use opam switch to lookup ocamlmerlin binary
    (setq merlin-command 'opam)))


;;
;; Haskell stuff
;;

(add-hook 'haskell-mode-hook
	  (lambda ()
	    (set (make-local-variable 'company-backends)
		 (append '((company-capf company-dabbrev-code))
			 company-backends))))

;; Key binding for commenting/uncommenting
(add-hook 'haskell-mode-hook
	  (lambda ()
	    (define-key haskell-mode-map (kbd "C-c ;") 'comment-or-uncomment-region)))

;; Always start the interactive mode
(add-hook 'haskell-mode-hook
	  (lambda ()
	    (interactive-haskell-mode)))

;; Type `C-c C-c` to compile the project
;; Type `C-c C-l` to "load" the file and reset the REPL
;; Type `C-c C-z' to toggle between the REPL and the code

;;
;; yasnippet
;;

;; FIXME: I've never actually used this...
(setq yas-snippet-dirs '("~/.emacs.d/snippets"))


;;
;; Org mode
;;

(defun my-org-minor-modes ()
  (org-bullets-mode)
  (flyspell-mode)
  (visual-line-mode))

(add-hook 'org-mode-hook 'my-org-minor-modes)

;; ;; Binding for emphasis like in AucTeX mode
;; ;; FIXME: This doesn't work. The corresponding AucTeX function `TeX-font` is complicated.
;; (add-hook 'org-mode-hook
;;	  (lambda ()
;;	    (print "tamtam")
;;	    (define-key org-mode-map (kbd "C-c C-f C-e") 'org-emphasize)
;;	    (define-key org-mode-map (kbd "C-c C-f C-i") (lambda () (org-emphasize ?/)))
;;	    (define-key org-mode-map (kbd "C-c C-f C-b") (lambda () (org-emphasize ?*)))
;;	    (define-key org-mode-map (kbd "C-c C-f C-t") (lambda () (org-emphasize ?=)))))

(add-hook 'org-mode-hook
	  (lambda ()
	    (define-key org-mode-map (kbd "C-c C-f") 'org-emphasize)))


; FIXME: "C-c C-j" is a bit confusing, since this is used in TeX to insert a new item
; In `org-mode`, just press "M-RET" to insert a new list item or headline (`org-meta-return`)

; ; Keybindings
; (add-hook 'org-mode-hook
;	  (lambda ()
;	    (define-key org-mode-map (kbd "C-c C-j") 'org-insert-item)
;	    (define-key org-mode-map (kbd "C-c C-g") 'org-goto)))






;;
;; Elisp mode
;;

(add-hook 'emacs-lisp-mode-hook 'company-mode)
(add-hook 'emacs-lisp-mode-hook 'comment-tags-mode)
(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (define-key emacs-lisp-mode-map (kbd "C-c ;") 'comment-or-uncomment-region)))



;;
;; Unicode math mode
;;


(define-minor-mode company-math-symbols-unicode-mode
  "This minor mode enables the `company-mode' frontent `company-math-symbols-unicode'."
  :init-value nil
  (cond
   (company-math-symbols-unicode-mode
    (add-to-list 'company-backends 'company-math-symbols-unicode))
   (t
    (setq company-backends
	  (delete 'company-math-symbols-unicode company-backends)))))

;;
;; Markdown mode
;;

(defun my-markdown-minor-modes ()
  (company-mode)
  (company-statistics-mode)
  (print "Now activating company-math-symbols-unicode-mode-mode ...")
  (company-math-symbols-unicode-mode)
  (flyspell-mode)
  (comment-tags-mode)
  (define-key markdown-mode-map (kbd "C-c ;") 'comment-or-uncomment-region)
  (define-key markdown-mode-map (kbd "<tab>") 'markdown-cycle))

(add-hook 'markdown-mode-hook 'my-markdown-minor-modes)

(defun my-xclip-region-html ()
  "Copy the HTML content of the current buffer to the clipboard using `xclip'."
  (let* ((process-connection-type nil)
	 (proc (start-process
		"xclip" nil "xclip" "-selection" "clipboard" "-t" "text/html")))
    (when proc
      (process-send-region proc (point-min) (point-max))
      (process-send-eof proc))))

(defun my-markdown-copy-html ()
  "Run Markdown on file and copy the HTML code to the xclipboard The
  document can then be pasted in any application that supports HTML,
  e.g. in Thunderbird."
  (interactive)
  (save-window-excursion
    (markdown)
    (with-current-buffer markdown-output-buffer-name
      (my-xclip-region-html))))

(add-hook 'markdown-mode-hook
	  (lambda ()
	    (define-key markdown-mode-command-map (kbd "h") 'my-markdown-copy-html)))

(add-hook 'markdown-mode-hook
	  (lambda ()
	    (define-key markdown-mode-command-map (kbd "h") 'my-markdown-copy-html)))

;;
;; imenu-list-mode
;;

;; Use `visual-line-mode' in the `imenu-list' buffer
(add-hook 'imenu-list-major-mode-hook
	  (lambda ()
	    (visual-line-mode)))


;;
;; comment-tag-mode
;;

(with-eval-after-load "comment-tags"
  (add-to-list 'comment-tags-keywords "FEEDBACK")
  (add-to-list 'comment-tags-keyword-faces
        '("FEEDBACK" :inherit info :foreground "mediumspringgreen"))
  (add-to-list 'comment-tags-keywords "NOTE")
  (add-to-list 'comment-tags-keyword-faces
        '("NOTE" :inherit info :foreground "gold"))
  (add-to-list 'comment-tags-keywords "WONTFIX")
  (add-to-list 'comment-tags-keyword-faces
               '("WONTFIX" :inherit error :weight bold))
  (add-to-list 'comment-tags-keywords "WIP")
  (add-to-list 'comment-tags-keyword-faces
               '("WIP" :inherit info :weight bold))
  (add-to-list 'comment-tags-keywords "IDEA")
  (add-to-list 'comment-tags-keyword-faces
               '("IDEA" :inherit info :weight bold))
  (add-to-list 'comment-tags-keywords "TEX")
  (add-to-list 'comment-tags-keyword-faces
               '("TEX" :inherit warning :weight bold)))

;;
;; Helm mode
;;


(add-hook 'helm-mode-hook
	  (lambda ()
	    ;; Rebind `C-x C-f` from `find-file` to `helm-browse-project`
	    ;; I usually use the evil `:edit` command to open files, so it is
	    ;; nice to use a seperate command for opening files in the (git) project
	    (global-set-key (kbd "C-x p") 'helm-browse-project)
	    ;; Note that This binding is usually bound to `set-fill-column', but I don't need this.
	    (global-set-key (kbd "C-x f") 'helm-find-files)
	    (global-set-key (kbd "M-x") 'helm-M-x)))

;; - "M-o" switches in between the "sources" (e.g. history and available commands)
;; - Don't press TAB!



;;
;; Misc
;;


;; Save the command history
;; I hope that works...
(setq savehist-mode 1)


;; From `ergoemacs-mode'
(defun new-empty-buffer ()
  "Opens a new empty buffer."
  (interactive)
  (switch-to-buffer (generate-new-buffer "untitled"))
  (funcall initial-major-mode)
  ; (put 'buffer-offer-save 'permanent-local t)
  (setq buffer-offer-save t))


;; Have some idea? Start writing some text quickly!
(defun quick-text ()
  (interactive)
  (new-empty-buffer)
  (writeroom-mode)
  (visual-line-mode)
  (flyspell-mode)
  (setq sentence-end-double-space nil)) ;; One space after period.

(add-hook 'writeroom-mode-hook 'visual-line-mode)


(require 'notifications)

;; A simple tea timer
;; Derived from `egg-timer.el'
;; TODO: Ask for a label; use the default label if the string was empty or nil
(defun tea-timer (minutes &optional label)
  "Schedule a timer to go off in MINUTES."
  (interactive "nHow many minutes? ")
  (run-at-time
   (format "%s minutes" minutes)
   nil
   (lambda ()
      (notifications-notify
       :title "Tea Timer"
       :body "Tea is ready!")))
  (message "Tea timer is scheduled to %d minutes" minutes))

;; FIXME: I think I need lexical bindings. The `label' doesn't work!
;; (defun tea-timer (minutes &optional label)
;;   "Schedule a timer to go off in MINUTES.
;; Provide LABEL to change the notifications, which defaults to \"MINUTES
;; minutes\"."
;;   (interactive "nHow many minutes? ")
;;   (if (or (not label) (string-empty-p label))
;;       (setq label (format "%s minutes" minutes))
;;     (setq label label))
;;   (run-at-time
;;    (format "%s minutes" minutes)
;;    nil
;;    (lambda ()
;;       (notifications-notify
;;        :title "Tea Timer"
;;        :body (or (and label (format "Tea-Timer (%s) is up!" label)) "Tea-Timer is up!"))))
;;   (message "Timer is scheduled to %d minutes" minutes))

;; Note: Alternatively, the function `org-timer-start-timer' can be used.
;; It displays the countdown in the mode line, but only one timer can be started.



;;
;; Global key bindings
;;

; (global-set-key (kbd "<Scroll_Lock>") #'centered-cursor-mode)



;; TODO: Maybe use `use-package`


;;
;; Not so sexy Stuff
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-label-alist
   '(("figure" . LaTeX-figure-label)
     ("table" . LaTeX-table-label)
     ("figure*" . LaTeX-figure-label)
     ("table*" . LaTeX-table-label)
     ("equation" . LaTeX-equation-label)
     ("eqnarray" . LaTeX-eqnarray-label)
     ("lemma" . "lem:")
     ("definition" . "def:")))
 '(TeX-auto-save t t)
 '(TeX-command-list
   '(("TeX" "%(PDF)%(tex) %(file-line-error) %(extraopts) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("LaTeXShellEscape" "%`%l -shell-escape %(mode)%' %t" TeX-run-TeX nil t)
     ("Makeinfo" "makeinfo %(extraopts) %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo %(extraopts) --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "amstex %(PDFout) %(extraopts) %`%S%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "%(cntxcom) --once --texutil %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "%(cntxcom) %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX")
     ("Biber" "biber %s" TeX-run-Biber nil t :help "Run Biber")
     ("View" "%V" TeX-run-discard-or-function t t :help "Run Viewer")
     ("Print" "%p" TeX-run-command t t :help "Print the file")
     ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     ("File" "%(o?)dvips %d -o %f " TeX-run-dvips t t :help "Generate PostScript file")
     ("Dvips" "%(o?)dvips %d -o %f " TeX-run-dvips nil t :help "Convert DVI file to PostScript")
     ("Dvipdfmx" "dvipdfmx %d" TeX-run-dvipdfmx nil t :help "Convert DVI file to PDF with dvipdfmx")
     ("Ps2pdf" "ps2pdf %f" TeX-run-ps2pdf nil t :help "Convert PostScript file to PDF")
     ("Glossaries" "makeglossaries %s" TeX-run-command nil t :help "Run makeglossaries to create glossary file")
     ("Index" "makeindex %s" TeX-run-index nil t :help "Run makeindex to create index file")
     ("upMendex" "upmendex %s" TeX-run-index t t :help "Run upmendex to create index file")
     ("Xindy" "texindy %s" TeX-run-command nil t :help "Run xindy to create index file")
     ("Check" "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     ("ChkTeX" "chktex -v6 %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for common mistakes")
     ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     ("Other" "" TeX-run-command t t :help "Run an arbitrary command")))
 '(TeX-quote-language-alist '(("british" "`" "'" t)))
 '(TeX-save-query nil)
 '(TeX-source-correlate-start-server t)
 '(TeX-view-program-selection '((output-pdf "Zathura")))
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(auto-insert-mode t)
 '(auto-revert-verbose nil)
 '(blink-cursor-mode nil)
 '(calendar-week-start-day 1)
 '(calendar-weekend-days '(6 0))
 '(comment-tags-require-colon nil)
 '(company-idle-delay 0.2)
 '(compilation-ask-about-save nil)
 '(coq-accept-proof-using-suggestion 'ignore)
 '(coq-prog-name "coqtop")
 '(custom-enabled-themes '(manoj-dark))
 '(evil-cross-lines nil)
 '(evil-undo-system 'undo-fu)
 '(evil-want-C-d-scroll t)
 '(evil-want-C-i-jump t)
 '(evil-want-C-u-scroll t)
 '(evil-want-C-w-delete t)
 '(evil-want-C-w-in-emacs-state t)
 '(evil-want-abbrev-expand-on-insert-exit t)
 '(font-latex-match-warning-keywords '("todo" "todoi"))
 '(font-latex-user-keyword-classes
   '(("todocmds"
      (("todo" "[{")
       ("todoi" "[{"))
      warning command)))
 '(global-auto-revert-mode t)
 '(global-hide-mode-line-mode nil)
 '(haskell-interactive-popup-errors nil)
 '(haskell-tags-on-save t)
 '(helm-completion-style 'emacs)
 '(helm-mode t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(ispell-dictionary "british")
 '(markdown-command '("pandoc" "--from=markdown" "--to=html5"))
 '(markdown-enable-wiki-links t)
 '(mouse-wheel-progressive-speed nil)
 '(org-agenda-start-on-weekday 1)
 '(org-hide-emphasis-markers nil)
 '(org-pretty-entities t)
 '(org-todo-keywords '((sequence "TODO" "WIP" "DONE" "NEW" "OLD" "IDEA")))
 '(package-selected-packages
   '(dap-mode rustic 0blayout elpher 2048-game egg-timer academic-phrases hide-mode-line highlight evil-surround nov company-reftex helm-ls-git helm org-bullets undo-fu toc-org olivetti wc-mode darkroom writeroom-mode evil-tutor comment-tags pandoc-mode markdown-toc imenu-list markdown-mode+ unicode-math-input company-statistics company-bibtex math-symbols company-math company-emoji utop evil visual-fill-column goto-last-change flycheck lsp-ui lsp-mode gnugo erlang company-ghc haskell-mode undo-tree clojure-mode-extra-font-locking company-auctex company-coq ##))
 '(proof-splash-time 0)
 '(proof-three-window-mode-policy 'hybrid)
 '(reftex-label-alist
   '(("definition" 116 nil nil nil
      ("Definition" "definition"))
     ("lemma" 116 nil nil nil
      ("Lemma" "lemma"))))
 '(reftex-section-levels
   '(("part" . 0)
     ("chapter" . 1)
     ("section" . 2)
     ("subsection" . 3)
     ("subsubsection" . 4)
     ("paragraph" . 5)
     ("subparagraph" . 6)
     ("addchap" . -1)
     ("addsec" . -2)
     ("frametitle" . -3)))
 '(safe-local-variable-values
   '((org-agenda-files
      '("structure.md"))
     (TeX-master . thesis)
     (ispell-dictionary . "british")))
 '(send-mail-function 'mailclient-send-it)
 '(sentence-end-double-space nil)
 '(show-paren-mode t)
 '(tex-offer-save nil)
 '(texmathp-tex-commands '(("mathpar" env-on) ("mathparpagebreakable" env-on)))
 '(tool-bar-mode nil)
 '(which-function-mode nil)
 '(writeroom-global-effects
   '(writeroom-set-fullscreen writeroom-set-alpha writeroom-set-tool-bar-lines writeroom-set-vertical-scroll-bars writeroom-set-bottom-divider-width))
 '(writeroom-mode-line t)
 '(writeroom-mode-line-toggle-position 'mode-line-format)
 '(writeroom-width 120))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 140 :width normal))))
 '(which-func ((t nil))))
;; FIXME: The above colour for `which-fun' doesn't work well with some themes.
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
