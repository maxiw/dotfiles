set nocompatible
filetype off

" TODO: Clean this up, on some other day
" TODO: Remove LaTeX, merlin etc. because I don't use this often in vim anyway

" Vundle Plugins
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'fatih/vim-go'
Plugin 'cypok/vim-sml'
Plugin 'dpelle/vim-LanguageTool'
Plugin 'lervag/vimtex'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'bohlender/vim-smt2'
call vundle#end()


filetype plugin indent on
syntax on

if has('gui_running')
	colorscheme darkblue
	set background=dark
	set guioptions-=T
	" set guifont=DejaVu\ Sans\ Mono\ For\ Powerline:h11
	set columns=980
	set lines=961
	" Disable all blinking:
	set guicursor+=a:blinkon0
else
	set t_Co=256
	set background=dark
endif

set laststatus=2
set noexpandtab
set nosmarttab tabstop=4 shiftwidth=4 softtabstop=4
set autoindent
set showcmd
set number
set showmatch
" set hlsearch
set incsearch
set ignorecase
set smartcase
set backspace=2
"set textwidth=79
set formatoptions=c,q,r,t
set ruler
set grepprg=grep\ -nH\ $*
set nocursorline
set wildmenu
set wildmode=list:longest,full
set spl=de,en
set noeb vb t_vb=
set undodir=$HOME/.vim/undo
set undofile
set undolevels=1000
set undoreload=10000
set mouse=a
set linebreak

set statusline+=%#warningmsg#
set statusline+=%*

let Tlist_Auto_Highlight_Tag = 1
let Tlist_Auto_Update = 1
let Tlist_Use_Right_Window = 1


let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='ravenpower'

" Detect if the current file type is a C-like language.
au BufNewFile,BufRead c,cpp,objc,*.mm call SetupForCLang()

au BufNewFile,BufRead *.minilisp set syntax=Scheme

" TODO: Fix this
" Configuration for C-like languages.
function! SetupForCLang()
	setlocal textwidth=80
	setlocal wrap

	" Use 2 spaces for indentation.
	setlocal shiftwidth=2
	setlocal tabstop=2
	setlocal softtabstop=2
	setlocal expandtab

	" Configure auto-indentation formatting.
	setlocal cindent
	setlocal cinoptions=h1,l1,g1,t0,i4,+4,(0,w1,W4
	setlocal indentexpr=GoogleCppIndent()
	let b:undo_indent = "setl sw< ts< sts< et< tw< wrap< cin< cino< inde<"
endfunction

" Do not highlight spelling mistakes in log files
au BufNewFile,BufRead *.log set nospell

" cd to the folder of the current file
noremap ,cd :lcd %:p:h<CR>:pwd<CR>

let g:tex_flavor = "latex"
" TeX compilation shortcut
au BufNewFile,BufRead *.tex :map <C-P> :w <CR> :!pdflatex -interaction nonstopmode %<CR><CR> :VimtexView <CR>

" Disable spell checking in TeX comments
autocmd BufEnter *.tex let g:tex_comment_nospell=1
autocmd BufEnter *.tex syn cluster texCommentGroup contains=texTodo,@NoSpell

let g:languagetool_jar='$HOME/languagetool/languagetool-commandline.jar'


" " Syntastic
" " set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
" " lacheck: filter out these space warnings...
" let g:syntastic_tex_lacheck_tail = "| grep --invert-match (space|Do not use @)"

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Haskell
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

" Tab specific option
function! SetupForHaskell()
	setlocal tabstop=8                   "A tab is 8 spaces
	setlocal expandtab                   "Always uses spaces instead of tabs
	setlocal softtabstop=4               "Insert 4 spaces when tab is pressed
	setlocal shiftwidth=4                "An indent is 4 spaces
	setlocal shiftround                  "Round indent to nearest shiftwidth multiple
endfunction

au BufNewFile,BufRead *.hs,*.hsl,*.cabal call SetupForHaskell()
" ## added by OPAM user-setup for vim / base ## 93ee63e278bdfc07d1139a748ed3fff2 ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_check_cmdline = ["opam list --installed --short --safe --color=never"] + s:opam_packages
let s:opam_available_tools = split(system(join(s:opam_check_cmdline)))
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if count(s:opam_available_tools, tool) > 0
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
